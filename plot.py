#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ========================================================== #

# __author__ = "Max Dopke"
# __copyright__ = ""
# __credits__ = ["Max Dopke, Raphael Bevand"]
# __license__ = ""
# __version__ = "0.0.1"
# __maintainer__ = "Max Dopke"
# __email__ = "m.f.dopke@tudelft.nl"
# __status__ = "Playground"
# __comments__ = "Feel free to share. Please leave header in"

# ========================================================== #

# sample usage
# python plot.py --show 'Confirmed - Recovered - Deaths'/'Confirmed'/'Deaths / Recovered' --countries Netherlands Spain Italy Total --increment True/False --shift True/False 

import numpy
import re
import io
import csv
import datetime
import argparse
import matplotlib.pyplot as plt
import urllib.request

def readfile(aCategory="Confirmed"):
    Path = [None, None, None]
    Path[0] = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19"
    Path[1] = "master/csse_covid_19_data/csse_covid_19_time_series"
    Path[2] = "time_series_covid19_%s_global.csv" % aCategory

    Content = urllib.request.urlopen("/".join(Path)).read().decode("utf-8")
    File = io.StringIO(Content)
    Read = csv.reader(File)

    Time = []
    for Date in Read.__next__()[4:]:
        Month, Day, Year = [int(Digit) for Digit in Date.split("/")]
        Time.append(datetime.date(Year, Month, Day))

    Data = {}
    for Columns in Read:
        State = Columns[0]
        Country = Columns[1]

        Counter = []
        for elem in Columns[4:]:
            if len(elem) == 0:
                Counter.append(0)
            else:
                Counter.append(int(elem))

        if len(State) == 0:
            State = Country

        # in the original routine states where separated
        if Country not in Data:
            Data[Country] = numpy.asarray(Counter)
        else:
            Data[Country] += numpy.asarray(Counter)



    #Statistics = dict(Time=Time, Data=Data)
    return Data, Time

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--scale', type=str, default='linear', help='linear/log: Choose between linear/log scale on the y axis')

    parser.add_argument('-H', '--histogram', action='store_true', default=False, help='True/False: Histograms of the maximums per country')
    parser.add_argument('-T', '--timeseries', action='store_true', default=False, help='True/False: Time series per selected country')
    parser.add_argument('-S', '--shift', action='store_true', default=False, help='True/False: Data can be shifted to (x,y) at y=ref defined by --ref')
    parser.add_argument('-D', '--increment', action='store_true', default=False, help='True/False: Plot only the increment in cases for each day')
    parser.add_argument('-C', '--percapita', action='store_true', default=False, help='True/False: Plot results per 100,000 inhabitants')

    parser.add_argument('-r', '--ref', type=float, default=10, help='Reference at which (x,y) y=ref is selected. Also used as reference to exclude countries with y < ref when selecting all')
    parser.add_argument('-c', '--countries', type=str, nargs='+', default='Netherlands', help='Country names to plot or "Total"')
    parser.add_argument('-s', '--show', type=str, default="'Confirmed - Deaths - Recovered'", help='Results to be displayed. Use quotation marks for mathematical operations.')

    args = parser.parse_args()

    #print(args)

    # read all the data
    Confirmed, datesC = readfile('confirmed')
    Deaths, datesD = readfile('deaths')
    Recovered, datesR = readfile('recovered')
    x = numpy.arange(len(datesC))

    # check inputs
    if any('all' in country for country in args.countries):
        args.countries = []
        for key in Confirmed.keys():
            args.countries.append(key)
        args.countries.append('Total')
    else:
        for country in args.countries:
            if country not in Confirmed.keys() and country != 'Total':
                print('')
                print('Country %s is not recognized.' % country )
                print('Please select a country form the list below or "Total"')
                print('List of available countries:')
                for key in Confirmed.keys():
                    print(key)
                print('')
                print('Country %s is not recognized.' % country )
                print('Please select a countries form the list above or "Total"')
                print('Separate all countries by a space')
                print('')
    
                exit()

    try:
        # evaluate operation
        op = {}
        data = {}
        data['Total'] = numpy.zeros(Confirmed['Netherlands'].shape)
        for country in Confirmed.keys():
            op[country] = args.show
            for var in ['Confirmed', 'Deaths', 'Recovered']:
                idx = [m.start() for m in re.finditer(var, op[country])][::-1]
                n = len(var)
                for m in idx:
                    op[country] = op[country][:m+n] + '["'+country+'"]' + op[country][m+n+1:]

            if args.percapita:
                import world_bank_data as wb
                try:
                    if country == 'US':
                        test_country = 'United States'
                    else:
                        test_country = country
                    population = wb.get_series('SP.POP.TOTL', mrv=1)[test_country][0]/100000
                    data[country] = eval(' ( ' + op[country] + ' ) / population')
                except:
                    print('No entry for %s in world data bank' % country)
                    data[country] = eval(op[country])
            else:
                data[country] = eval(op[country])
            data['Total'] += data[country]

    except:
        print('')
        print('Could not evaluate string %s' % args.show)
        print('Make sure the string is composed of variables:')
        print('Confirmed, Deaths and Recovered')
        print('and mathematical symbols:')
        print('+, -, * and /')
        print('All variables and mathematical symbols have to be separated by a space')
        print('')

        exit()

    # plot data
    if args.shift:
        min_ = 999999 
        for country in args.countries:
            if args.increment:
                y = list(numpy.diff(data[country])[::-1])
                y.append(data[country][0])
                y = y[::-1]
            else:
                y = data[country]
            start = numpy.where(numpy.abs(numpy.asarray(y) - args.ref) == min(numpy.abs(numpy.asarray(y) - args.ref)))[0][0]
            if start < min_:
                min_ = start



    if args.timeseries:

        ls = ['-', '--', '-.', ':']
        ms = ['s', 'o', 'd', 'p']
        l = 0
        m = 0

        fig, ax = plt.subplots()
        max_ = 0
        for i, country in enumerate(args.countries):
            m += 1
            if (i)%4 == 0:
                m = 0
                l += 1
            if (i)%16 == 0:
                l = 0

            if args.increment:
                y = list(numpy.diff(data[country])[::-1])
                y.append(data[country][0])
                y = y[::-1]
            else:
                y = data[country]

            if (len(args.countries) == len(Confirmed.keys())+1 and numpy.max(y) >= args.ref) or len(args.countries) != len(Confirmed.keys())+1:
                if args.shift:
                    start = numpy.where(numpy.abs(numpy.asarray(y) - args.ref) == min(numpy.abs(numpy.asarray(y) - args.ref)))[0][0] - min_
                    ax.plot(x - start, y, linestyle=ls[l], marker=ms[m], label=country + ' +%d days' % (start))
                else:
                    ax.plot(x, y, linestyle=ls[l], marker=ms[m], label=country)
    
            __max__ = max(y)
            if __max__ >  max_:
                max_ = __max__
    
        ax.set_ylabel(args.show)
        ax.set_xticks(x[::7])
        ax.set_xticklabels(datesC[::7], rotation=45)
        ax.set_yscale(args.scale)
        if args.scale == 'log':
            ax.set_ylim(10, max_)
        ax.set_xlim(0, max(x))
        if args.shift:
            ax.title.set_text('Reference for shift %d' % args.ref)
        ax.legend(loc='upper left', numpoints=1)

        fig.subplots_adjust(left=None, bottom=0.2, right=None, top=None, wspace=None, hspace=None)

    if args.histogram:
        # histogram with countries maximums
        x = numpy.arange(len(args.countries))
        y = numpy.zeros(len(x))
        for i, key in enumerate(args.countries):
            if args.increment:
                y[i] = data[key][-2] - data[key][-3]
            else:
                y[i] = data[key][-1]
    
        indices = numpy.argsort(y)[::-1]
        y = y[indices]

        xlim = numpy.where(numpy.abs(y - args.ref) == min(numpy.abs(y - args.ref)))[0][0]
    
        labels = []
        for j, i in enumerate(indices):
            if j < xlim:
                labels.append(args.countries[i])
    
        fig, ax = plt.subplots()
    
        ax.bar(x, y, align='center')
    
        ax.set_xlim(-1, xlim)
        ax.set_ylim(0, 1.1*numpy.max(y))
    
        ax.set_xticks(x[:xlim])
        ax.set_xticklabels(labels, rotation=60)
        ax.set_ylabel(args.show)
        fig.subplots_adjust(left=None, bottom=0.2, right=None, top=None, wspace=None, hspace=None)

    plt.show()


    
